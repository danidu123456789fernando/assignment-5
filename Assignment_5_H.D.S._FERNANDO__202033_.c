 #include<stdio.h>
 
 /*Cost Occurred (constants)*/
 #define PerformanceFixedCostRupees 500
 #define AdditionalCostperAttendeeRupees 3
 
 /*functions for costs*/
 int Totalnumberofheads(int Price);
 int income(int Price);
 int cost(int Price);
 int profit(int Price);
 
 int Totalnumberofheads(int Price){
 	return 120-(Price-15)/5*20;
 }
 
 int income(int Price){
 	return Totalnumberofheads(Price)*Price;
 }

  
 int cost(int Price){
  	return Totalnumberofheads(Price)*AdditionalCostperAttendeeRupees+PerformanceFixedCostRupees;
  }
  
  int profit(int Price){
  	return income(Price)-cost(Price);
  }
  
  int main(){
  	int Price;
  	printf("\nprofit Expected for Tickets: \n\n");
  	for(Price=5;Price<50;Price+=5)
  	{
  		printf("Ticket Price = Rs.%d \t\t profit = Rs.%d\n",Price,profit(Price));
  		printf("*****************************************************\n\n");
  		
	  }
	  printf("CHOOSE THE PRICE WITH THE HIGHEST PROFIT FROM THE ABOVE LIST AS THE TICKET PRICE");
	  return 0;
  }
  
  
